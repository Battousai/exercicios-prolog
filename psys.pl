/*

Production system

2000/03/28

*/


/*
% (LPA) Win Prolog
:- prolog_flag(unknown, _, fail).
*/

% SWI Prolog
:-unknown(_, fail).


:- op(10, xfy, then).
:- op(100, fx, if).


:- op(1000, xfy, and).
:- op(1100, xfy, or).


and(X, Y):-
	call(X), call(Y).

or(X, Y):-
	call(X); call(Y).




% Inference engine

psys :-
	repeat,
		conflict_set(Set),
		process_conflict_set(Set), !.

process_conflict_set([]):- !.
process_conflict_set(Set):-
	selected_action(Set, Action),
	perform_action(Action),
	!,
	fail.

conflict_set(Set):-
	findall(rule(Cond, Act), (rule(Cond, Act), satisfied(Cond)), Set).

% simple minded conflict resolution strategy
selected_action([rule(_, Act)|_], Act).


% simple, but maybe sufficient, condition evaluator
satisfied(Cond):- call(Cond).

% simple, but maybe sufficient, action executer
perform_action(Action):- call(Action).


rule(A, B):- if A then B.



% USEFUL STUFF
%
% clearMemory(FactsPredicate):	Removes all clauses of the predicates specified
%				in the list maintained in the fact FactsPredicate
%
clearMemory(FactsPredicate) :-
	FACTS =.. [FactsPredicate, Facts],
	call(FACTS),
	clearFacts(Facts).
clearFacts([P/N | Fs]):-
	functor(Fact, P, N),
	retractall(Fact),
	clearFacts(Fs).
clearFacts([]).


list_rubbish(RubbishHoldingPred):-
	RUBBISHHOLDINGPRED =.. [RubbishHoldingPred, Rubbish],
	call(RUBBISHHOLDINGPRED),
	list_facts(Rubbish).


list_facts([P/N|Rubbish]):-
	functor(Fact, P, N),
	print_matching_facts(Fact),
	list_facts(Rubbish).
list_facts([]).


print_matching_facts(Fact):-
	call(Fact),
	write(Fact), nl,
	fail.
print_matching_facts(_).



writelist([X|Rest]):-
	write(X),
	writelist(Rest).
writelist([]).


























