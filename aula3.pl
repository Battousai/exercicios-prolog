trabalhador(alves).
trabalhador(ana).
trabalhador(matos).

tarefa(matar_baratas).
tarefa(introduzir_dados_sis_doc).
tarefa(limpar).
tarefa(lavarMaos).

nHoras(matar_baratas, 10).
nHoras(introduzir_dados_sis_doc, 5).
nHoras(lavarMaos, 1).
nHoras(limpar, 4).

tarefa_atribuida(alves, matar_baratas).
tarefa_atribuida(ana, introduzir_dados_sis_doc).
tarefa_atribuida(matos, lavarMaos).
tarefa_atribuida(matos, limpar).

% c
horas_tarefa_atribuida(Quem, Tarefa, Horas):-
    tarefa_atribuida(Quem, Tarefa),
    nHoras(Tarefa, Horas).

responsavel(matos, ana).
responsavel(ana, alves).
responsavel(mauro, matos).
responsavel(matos, joaquim).

% e
chefe(Chefe):-
    responsavel(Chefe, _),
    \+ responsavel(_, Chefe).

% d
horas_atribuidas(Quem, Horas):-
    findall(H, horas_tarefa_atribuida(Quem, _, H), Bag),
    sl(Bag, Sum),
    Horas is Sum.
    

% sum_list equivalent
sl([H|L], S):-
    sl(L, Aux),
    S is H + Aux.
sl([], 0).


    
    
    
    
    
    
    
    
    
    
    
    
    
