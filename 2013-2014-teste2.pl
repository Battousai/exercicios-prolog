/*

?- list_with_error([a, b, c, d], L).
L = [a, d, c, b]

*/

list_with_errors(L1, L2):-
  is_list(L1),
  var(L2),
  length(L1, Len1),
  N is Len1 + 1,
  random(1, N, Pos1),
  random(1, N, Pos2),
  nth(L1, Pos1, X1),
  nth(L1, Pos2, X2),
  modified_list(L1, [(Pos1, X2), (Pos2, X1)], L2).


modified_list(L, [], L):- !.
modified_list(L1, [(Pos, Val)|Changes], L2):-
  modify(L1, Pos, Val, L),
  modified_list(L, Changes, L2).

modify([], _, _, []).
modify([_|L], 1, Val, [Val|O]):-
  modify(L, 0, _, O).
modify([H|L], Pos, Val, [H|O]):-
  Pos1 is Pos - 1,
  modify(L, Pos1, Val, O).

nth([H|_], 1, H).
nth([_|L], Index, H):-
  NewIndex is Index - 1,
  nth(L, NewIndex, H).




/*
Escreve o programa print_lists_with_errors/1 que lê um ficheiro de listas, cada uma numa linha, terminada por um ponto.

Por cada lista lida, chama o predicado list_with_error/2 (definido na pergunta 1) e imprime a lista original e a lista com erro. O procedimento não faz validações. Mesmo que não tenhas definido o predicado list_with_errors/2, usa-o como se ele já existisse.

Exemplo

?- print_lists_with_errors(‘Lists.txt’).
[a, b, c, d] error: [a, b, c, d] % por acaso, a lista ficou igual
[1, 2, 3, 4] error: [1, 4, 3, 2]
[n, l, k, r] error: [n, k, l, r]
true
*/

print_lists_with_errors(InFile):-
  see(InFile),
    repeat,
      read(List),
      process_input_list(List),
    !,
  seen.

process_input_list(end_of_file).
process_input_list(Original):-
  list_with_errors(Original, Error),
  print_error_list(Original, Error),
  !,
  fail.
print_error_list(Original, Error):-
  write(Original), write(" error: "), write(Error),
  nl.




/*
Admite que tens uma série de factos list/1 que armazenam listas, por exemplo list([a, b, c, d, e]).

Escreve o predicado print_lists_with_errors/0 que, para cada lista armazenada no predicado list/1, chama o predicado list_with_error/2 (definido na pergunta 1) e imprime a lista original e a lista com erro.

O procedimento não faz validações. Mesmo que não tenhas definido o predicado list_with_errors/2, usa-o como se ele já existisse.

*/

lists([a,b,c,d,e]).
lists([1,2,3,4,5]).
lists([ab,cd,ef,gh]).

print_lists_with_error:-
  lists(L),
  printerrorlistof(L).
print_lists_with_error.
printerrorlistof(L):-
  list_with_errors(L, E),
  print_error_list(L, E),
  %!,
  fail.

print_lists_with_errors :-
  lists(L1),
  list_with_errors(L1, L2),
  write(L1), tab(4), write('error: '), write(L2), nl,
  fail.
print_lists_with_errors.
