
member(X, [_|L]):-
  % X \= H,
  member(X, L).
member(X, [X|_]).
% member(X, [X|_]):- !.
% member(_, []).
% member(X, [Y|L]):-
%   X \= Y,
%   member(X, L).

append([], L, L).
append([H|L], L1, [H|O]):-
  append(L, L1, O).

first([X|_], X).

last([X], X).
last([_|L], X):-
  last(L, X).

reverse(L, L1):-
  reverse(L, [], L1).
reverse([], L, L).
reverse([H|L], L1, L2):-
  reverse(L, [H|L1], L2).
