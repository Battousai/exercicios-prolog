:- ensure_loaded("production_system_initial_class.pl").

% reposicionar caixas

if (
    current_position(Box, Pos) and
    desired_position(Box, DesiredPos) and
    Pos \= DesiredPos and
    \+ current_position(_, DesiredPos)
) then (
    remove_box(Box, Pos),
    place_box(Box, DesiredPos)
).

if (
    current_position(Box, Pos) and
    desired_position(Box, DesiredPos) and
    Pos \= DesiredPos and
    current_position(Box1, DesiredPos) and
    Box \= Box1
) then (
    remove_box(Box1, DesiredPos),
    remove_box(Box, Pos),
    place_box(Box, DesiredPos),
    place_box(Box1, Pos)
).


% ordenar conjunto de valores numéricos


if (
    number(X1, Pos1) and number(X2, Pos2) and
    X1 > X2 and Pos1 < Pos2
) then (
    retract(number(X1, Pos1)), retract(number(X2, Pos2)),
    assert(number(X1, Pos2)), assert(number(X2, Pos1)),
    writelist(['Switched ', X1, ' and ', X2, '.']), nl
).


% display sorted numbers


if (current_index(Pos) and number(N, Pos))
then (
    write(N), nl,
    retract(current_index(Pos)),
    Next is Pos + 1,
    assert(current_index(Next))
).

if (current_index(Pos) and \+ number(_, Pos))
then (
    retract(current_index(Pos))
).


number(3, 2).
number(10, 5).
number(-1, 1).
number(4, 3).
number(8, 4).


% reservoir dog


%bucket_capacity(b1, 30).
%bucket_capacity(b2, 20).
%bucket_capacity(b3, 10).

if (bucket_state(B, empty)
) then (
    fill_bucket(B),
    writelist('Filled bucket ', B),
    nl
).

if (
    desired_level(DL) and
    reservoir_level(L) and
    DL > L and
    ToFill is DL - L and
    bucket_capacity(B, Cap) and
    bucket_state(B, full),
    ToFill >= Cap and
    \+ (
        bucket_capacity(B1, C1) and
        bucket_state(B1, full) and
        ToFill >= C1 and
        C1 > Cap
    )
) then (
    dump_bucket(B)
).























