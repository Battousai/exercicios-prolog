familia(pereira).
familia(martins).

prenda(ana, livro, 10, pereira).
prenda(rui, dvd, 15, pereira).
prenda(sara, colar, 18, martins).

prendas_familias(L):-
  findall(F, familia(F), Fams),
  prendas(Fams, L).

prendas([], []).
prendas([F|T], [F:Prendas|L]):-
  findall(Prenda, prenda(_, Prenda, _, F), Prendas),
  prendas(T, L).
