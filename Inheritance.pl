/*
generalizacao(C1, C2): C1 � uma generaliza��o de C2

regra
C1 � uma generalizacao de C2 se C1 for uma superclasse de C2
C1 � uma generaliza��o de C2 se C1 for uma superclasse de uma
generaliza��o de C2

Lu�s
2015/09/25
*/

generalizacao(Super, Classe):- superclasse(Super, Classe).

generalizacao(C1, C2) :- superclasse(C1, C), generalizacao(C,C2).

% Exemplo de hierarquia de classes
%
superclasse(a, b1).
superclasse(a, b2).
superclasse(a, b3).

superclasse(b1, c1).
superclasse(b1, c2).

superclasse(b2, c3).

superclasse(b3, c4).
superclasse(b3, c5).

superclasse(c1, d).








