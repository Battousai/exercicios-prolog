disk(d).
disk(e).

stored(f1, d).
stored(f2, d).
stored(f3, d).
stored(f4, e).
stored(f5, e).

size(f1, 15).
size(f2, 0.5).
size(f3, 200).
size(f4, 200).
size(f5, 45).

total_stored(Disk, Total):-
    disk(Disk),
    assert(used(0)),
    used_space(Disk),
    retract(used(Total)).

used_space(Disk):-
    stored(F, Disk),
    size(F, S),
    update_sum(used, S),
    fail.
used_space(_).

update_sum(Which, By):-
    Sum =.. [Which, S],
    retract(Sum),
    Total is S + By,
    Next =.. [Which, Total],
    assert(Next).










