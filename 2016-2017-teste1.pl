:- ensure_loaded('psys.pl').


if (goal(average_computed) and \+ input(_))
then (
  write('enter a number or end: '),
  read(Input),
  assert(input(Input))
).

if (goal(average_computed) and input(end))
then (
  sum(S),
  count(C),
  write(C), tab(4), write(S),
  retract(goal(_)),
  retract(input(end)),
  retract(sum(S)),
  retract(count(C))
).

if (goal(average_computed) and input(I) and number(I))
then (
  update_count,
  update_sum(I),
  retract(input(I))
).
