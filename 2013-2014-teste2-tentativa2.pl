modified_list(L, Changes, R):-
  modified_list(L, Changes, R, 0).
modified_list([], _, [], _).
modified_list([H|T], Changes, [H|L], N):-
  \+ member((N, _), Changes),
  Next is N + 1,
  modified_list(T, Changes, L, Next).
modified_list([_|T], Changes, [H|L], N):-
  member((N, H), Changes),
  Next is N + 1,
  modified_list(T, Changes, L, Next).


greatest(D1, D2, D1):- D1 >= D2, !.
greatest(D1, D2, D2):- D2 > D1, !.
polynomial_degree(V, P1+P2, D):-
  !,
  polynomial_degree(V, P1, D1),
  polynomial_degree(V, P2, D2),
  greatest(D1, D2, D).
polynomial_degree(V, _*V^D, D):- !.
polynomial_degree(V, V^D, D):- !.
polynomial_degree(V, V, 1):- !.

sometest([E,E|T], [coiso|T]):- !.
sometest([E1,E2|T], [E1,E2|L]):-
  !,
  sometest(T, L).
sometest([E], [E]):- !.
sometest([], []):- !.
