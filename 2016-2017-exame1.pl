num(1).
num(2).
num(3).
num(4).
num(5).

somar_numeros(_):-
  assert(soma(0)),
  num(X),
  atualizar_soma(X),
  fail.

somar_numeros(Soma):-
  retract(soma(Soma)).

atualizar_soma(X):-
  retract(soma(S)),
  S1 is S + X,
  assert(soma(S1)),
  !.

sala('Joaquim').
sala('Andrade').

lugar('Andrade', a1).
lugar('Andrade', a2).
lugar('Andrade', a3).
lugar('Andrade', a4).
lugar('Andrade', a5).

lugar('Joaquim', a1).
lugar('Joaquim', a2).
lugar('Joaquim', a3).
lugar('Joaquim', a4).

lugar_vendido('Andrade', a1).
lugar_vendido('Andrade', a2).
lugar_vendido('Andrade', a3).
lugar_vendido('Andrade', a4).
lugar_vendido('Andrade', a5).

ls(Sala):-
  sala(Sala),
  lugar_vendido(Sala, _),
  fail.
ls(_).

lotacao_esgotada(Sala):-
  sala(Sala),
  \+ (
    lugar(Sala, Lugar),
    \+ lugar_vendido(Sala, Lugar)
  ),
  fail.
lotacao_esgotada(_).
