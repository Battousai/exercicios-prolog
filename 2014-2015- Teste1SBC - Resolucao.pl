:- ensure_loaded("psys.pl").

ingrediente_disponivel(leiteCoco).
ingrediente_disponivel(cominhos).
ingediente_disponivel(azeite).

ingrediente_necessario(caril, leiteCoco).
ingrediente_necessario(caril, cominhos).
ingrediente_necessario(caril, azeite).

ingrediente_necessario(bitoque, bife).
ingrediente_necessario(bitoque, ovo).


ingrediente_em_falta(Coz, Ingr):-
    ingrediente_necessario(Coz, Ingr),
    \+ ingrediente_disponivel(Ingr).


aval_cozinhado(caril, 3).
aval_cozinhado(bitoque, 2).
aval_cozinhado(castanhas, 4).
aval_cozinhado(pizza, 5).

if (
    \+ melhor_cozinhado(_) and
    aval_cozinhado(Coz, Av) and
    \+ (
        aval_cozinhado(_, Av2) and
        Av2 > Av
    )
) then (
    assert(melhor_cozinhado(Coz))
).


ingredientes_loja(bife, l1).
ingredientes_loja(castanhas, l1).
ingredientes_loja(azeite, l2).
ingredientes_loja(aluminio, l3).

if (
    melhor_cozinhado(C) and
    ingrediente_em_falta(C, Ingr) and
) then (















