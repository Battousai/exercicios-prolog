last(L1, L2, X):-
  reverse(L1, L2),
  first(L2, X).

first([H|_], H).

l([], L, L, _).
l([E], L, L2, E):-
  l([], [E|L], L2, E).
l([H|T], L, L2, E):-
  l(T, [H|L], L2, E).
l(L1, L2, X):-
  l(L1, [], L2, X).


type(X, atom):- atom(X), !.
type(X, list):- is_list(X), !.
type(X, number):- number(X), !.

types([H|T], [Type|L]):-
  types(T, L),
  type(H, Type),
  \+ member(Type, L).
types([H|T], L):-
  types(T, L),
  type(H, Type),
  member(Type, L).
types([], []).

num(1).
soma(0).

terms_list([1,2,3,a,[]]).
terms_list([1,2,3,a,[]]).
terms_list([4,5,[],6]).
terms_list([4,5,[],6]).

print_types:-
  terms_list(L),
  write(L), nl,
  types(L, Types),
  percorre_lista(Types),
  fail.
print_types.

percorre_lista([Type|Types]):-
  tab(4), write(Type), nl,
  percorre_lista(Types).
percorre_lista([]).


pt:-
  setof(L, terms_list(L), Lists),
  printLists(Lists).

printLists([L|R]):-
  write(L), nl,
  types(L, Types),
  printTypes(Types),
  printLists(R).
printLists([]).

printTypes([Type|Types]):-
  tab(4), write(Type), nl,
  printTypes(Types).
printTypes([]).

% reverse([], []).
% reverse(L, F):-
%   reverse(L, [], F).
% reverse([X|L], Aux, F):-
%   reverse(L, [X|Aux], F).
% reverse([], Aux, Aux).


expansao([], []).
expansao([(N, X)|L], E):-
  expansao(N, X, E),
  expansao(L, E).

expansao(N, X, [X|E]):-
  N > 0,
  N1 is N - 1,
  expansao(N1, X, E).
expansao(0, _, _).


replace(_, _, [], []):- !.
replace(Old, New, [Old|L1], [New|L2]):-
  !,
  replace(Old, New, L1, L2).
replace(Old, New, [H|L1], [H|L2]):-
  !,
  replace(Old, New, L1, L2).


nXList(1, C, [C]):- !.
nXList(N, C, [C|L]):-
  N > 1, !,
  Next is N - 1,
  nXList(Next, C, L).


nList(N, L):-
  nList(N, [], L).
nList(N, L1, L2):-
  N > 0, !,
  Next is N - 1,
  nList(Next, [N|L1], L2).
nList(0, L, L).


nLeftList(0, _, []).
nLeftList(N, [H|L1], [H|L2]):-
  Next is N - 1,
  nLeftList(Next, L1, L2).
%
% nRightList(N, L1, L2):-
%   reverse(L1, L3),
%   nLeftList(N, L3, L2).

nRightList(N, L1, L2):-
  length(L1, Len),
  St is Len - N,
  nRightListAux(St, L1, L2).

nRightListAux(0, [], []).
nRightListAux(0, [H|L], [H|L2]):-
  !,
  nRightListAux(0, L, L2).
nRightListAux(N, [_|L], L2):-
  N > 0, !,
  Next is N - 1,
  nRightListAux(Next, L, L2).

inteiros(L, I):-
  is_list(L),
  (is_list(I); var(I)),
  inteiros2(L, I).
inteiros2([H|L], [H|I]):-
  integer(H), !,
  inteiros2(L, I).
inteiros2([H|L], I):-
  \+ integer(H), !,
  inteiros2(L, I).
inteiros2([], []).


media(L, M):-
  is_list(L),
  is_numeric(L),
  (var(M); number(M)),
  sum(L, S),
  length(L, Len),
  M is S / Len.

is_numeric([H|T]):-
  number(H),
  is_numeric(T).
is_numeric([]).

sum([H|T], S):-
  sum(T, S1),
  S is H + S1.
sum([], 0).


exp([], []).
exp([(N, C)|T], [C|L]):-
  N > 0, !,
  Next is N - 1,
  exp([(Next, C)|T], L).
exp([(0, _)|T], L):-
  exp(T, L).


max([H|T], H):-
  max(T, M),
  H >= M.
max([H|T], M):-
  max(T, M),
  M > H.
max([H], H).

m(L, M):-
  m(L, 0, M).
m([H|T], S, M):-
  H >= S,
  m(T, H, M).
m([H|T], S, M):-
  S > H,
  m(T, S, M).
m([], M, M).

symmetric(C, S):-
  char_code(C, Dec),
  Diff is Dec - 97,
  NewCharCode is 122 - Diff,
  char_code(S, NewCharCode).

symmetric_list([H|T], [C|L]):-
  symmetric(H, C),
  symmetric_list(T, L).
symmetric_list([], []).

list_count([], 0).
list_count([_|T], N):-
  list_count(T, N1),
  N is N1 + 1.

xListMultiply(N, [H|T], [M|L]):-
  M is N * H,
  xListMultiply(N, T, L).
xListMultiply(_, [], []).

listProduct(L1, L2, L3):-
  length(L1, Len1), length(L2, Len2),
  Len1 = Len2,
  listProduct2(L1, L2, L3).
listProduct2([P1|L1], [P2|L2], [P3|L3]):-
  P3 is P1 * P2,
  listProduct2(L1, L2, L3).
listProduct2([], [], []).


power(X, N, P):-
  N > 0, !,
  Next is N - 1,
  power(X, Next, P1),
  P is P1 * X.
power(_, 0, 1).

arc(n1, n2).
arc(n1, n3).
arc(n1, n4).
arc(n2, n4).
arc(n2, n3).
arc(n3, n5).
arc(n5, n6).

connected(N1, N2):-
  arc(N1, N2).
connected(N1, N2):-
  arc(N1, N3),
  connected(N3, N2).
% connected(N1, N2):-
%   arc(N1, N3),
%   \+ arc(N3, N2),
%   connected(N3, N2).

fatorial(0, 1).
fatorial(N, S):-
  N > 0, !,
  Next is N - 1,
  fatorial(Next, S1),
  S is N * S1.

soma([H|L], S):-
  soma(L, S1),
  S is S1 + H.
soma([], 0).

getRandom(X, Y):-
  random(1, 5, X),
  random(1, 5, Y)
  .
rr:-
  getRandom(X, Y),
  write(X:Y), nl,
  fail.

disk(d).
disk(e).

stored(f1, d).
stored(f2, d).
stored(f3, d).
stored(f4, e).
stored(f5, e).

size(f1, 15).
size(f2, 0.5).
size(f3, 200).
size(f4, 200).
size(f5, 45).

espaco_ocupado(D, _):-
  assert(occ(0)),
  disk(D),
  stored(F, D),
  size(F, Sc),
  incr_occ(Sc),
  fail.
espaco_ocupado(_, S):-
  retract(occ(S)).

incr_occ(S):-
  retract(occ(S1)),
  S2 is S1 + S,
  assert(occ(S2)).

eo(D, S):-
  findall(D, disk(D), Disks),
  sizeOfDisks(Disks, S).
sizeOfDisks([D|Ds], S):-
  sizeOfDisks(Ds, S1),
  findall(F, stored(F, D), Fs),
  sizeOfFiles(Fs, S2),
  S is S1 + S2.
sizeOfDisks([], 0).
sizeOfFiles([F|Fs], S):-
  sizeOfFiles(Fs, S1),
  size(F, S2),
  S is S1 + S2.
sizeOfFiles([], 0).

modify([_|L], 1, Val, [Val|L]):- !.
modify([H|L], Pos, Val, [H|O]):-
  Pos1 is Pos - 1,
  modify(L, Pos1, Val, O).


sala(s1).
sala(s2).
sala(s3).

lugar(s1, 1).
lugar(s1, 2).
lugar(s2, 1).
lugar(s2, 2).
lugar(s3, 1).

lugar_vendido(s1, 1).
lugar_vendido(s2, 2).

lotacao_esgotada(_):-
  assert(cont(0)),
  sala(S),
  \+ (
    lugar(S, L),
    \+ lugar_vendido(S, L)
  ),
  incr_sala,
  fail.
lotacao_esgotada(S):-
  retract(cont(S)).

incr_sala:-
  retract(cont(S)),
  S1 is S + 1,
  assert(cont(S1)).
