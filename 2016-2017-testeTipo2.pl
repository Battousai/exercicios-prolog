xListMultiply(_, [], []).
xListMultiply(M, [H|T], [R|L]):-
  R is M * H,
  xListMultiply(M, T, L).


listProduct([H|L1], [I|L2], [R|L]):-
  R is H * I,
  listProduct(L1, L2, L).
listProduct([], [], []).


power(_, 0, 0).
power(X, N, P):-
  N > 0,
  Next is N - 1,
  power(X, Next, S),
  P is S + X * X.

%
arc(n1, n2).
arc(n1, n3).
arc(n2, n3).
arc(n3, n5).
arc(n5, n6).
arc(n6, n7).

connected(N, X):-
  arc(N, X).
connected(N, X):-
  arc(Sup, X),
  connected(N, Sup).

% ou

con(N, X):- arc(N, X).
con(N, X):-
  arc(N, N2),
  con(N2, X).


% printConnected

printConnected(N):-
  connected(N, Con),
  write(N), write('->'), write(Con), nl,
  fail.
printConnected(_).



% readNodes

readNodes:-
  repeat,
    write('Node: '), read(N),
    writeNode(N),
  !.
writeNode(end_of_nodes):- !.
writeNode(N):-
  assert(node(N)),
  fail.


% contar frequencia relativa
relative_frequences(L, O):-
  count_frequences(L, [], OutList, Sum),
  calculate_frequences(OutList, Sum, O).

calculate_frequences([], _, []).
calculate_frequences([(E, C)|L], Sum, [(E, Freq)|L1]):-
  Freq is C / Sum,
  calculate_frequences(L, Sum, L1).
count_frequences([], C, C, 0).
count_frequences([H|L], L1, OutList, S):-
  \+ member((H, _), L1),
  count_frequences(L, [(H, 1)|L1], OutList, S1),
  S is S1 + 1.
count_frequences([H|L], L1, OutList, S):-
  select((H, C), L1, L2),
  C1 is C + 1,
  count_frequences(L, [(H, C1)|L2], OutList, S1),
  S is S1 + 1.


% comprimir uma lista

compressed(L, L1):-
  cfreqs(L, [], Out),
  normalize(Out, L1).

normalize([], []).
normalize([(C, H)|L], [(C, H)|Out]):-
  C > 1, !,
  normalize(L, Out).
normalize([(C, H)|L], [H|Out]):-
  C = 1, !,
  normalize(L, Out).

cfreqs([], Out, Out).
cfreqs([H|L], L1, Out):-
  \+ member((_, H), L1),
  !,
  cfreqs(L, [(1, H)|L1], Out).
cfreqs([H|L], L1, Out):-
  select((C, H), L1, L2),
  C1 is C + 1,
  cfreqs(L, [(C1, H)|L2], Out).

% simplificar polinómios

degree_terms(K*Var^N, Var, N, [K*Var^N|L]).
degree_terms(Term1+Term2, Var, Deg, Terms):-
  degree_terms(Term1, Var, Deg, T1),
  degree_terms(Term2, Var, Deg, T2),

degree_terms(Terms, Var, Deg, )
