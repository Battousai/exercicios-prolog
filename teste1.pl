:- ensure_loaded("psys.pl").

/*
computar a média com regras de produção
predicados fornecidos:
    - read_number(X): lê número ou 'end'
    - update_count: substitui o facto count(N) por count(N1),
        onde N1 = N + 1
    - update_sum(X): substitui o facto sum(S) por sum(S1),
        onde S1 = S + X
*/

read_number(X):-
    write("insira um numero: "),
    read(X).

update_sum(X):-
    \+ sum(_),
    !,
    assert(sum(X)).
update_sum(X):-
    sum(S),
    !,
    S1 is S + X,
    retract(sum(S)),
    assert(sum(S1)).

update_count:-
    \+ count(_),
    !,
    assert(count(1)).
update_count:-
    count(C),
    !,
    C1 is C + 1,
    retract(count(C)),
    assert(count(C1)).

compute_average :-
    assert(goal(average_computed)), % Se não existir, as regras não trabalham
    psys.

if (goal(average_computed) and \+ doit)
then (assert(doit)).

if (\+ goal(average_computed) and doit)
then (
    retract(doit),
    retract(count(_)),
    retract(sum(_)),
    retract(number_read(_))
).

if (doit and \+ number_read(_))
then (
    read_number(X),
    assert(number_read(X))
).

if (doit and number_read(X) and X \= end)
then (
    update_sum(X),
    update_count,
    retract(number_read(X))
).

if (doit and number_read(end) and count(_))
then (
    sum(X),
    count(N),
    Av is X / N,
    write("média: "),
    write(Av),
    nl,
    retract(goal(average_computed)),
    retract(number_read(end))
).

if (doit and number_read(end) and \+ count(_))
then (
    write("nenhum número introduzido"),
    nl,
    retract(goal(average_computed)),
    retract(number_read(end))
).














