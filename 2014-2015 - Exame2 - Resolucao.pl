
/*
tem_alma(X):-
    javali(X),
    sentado(X, esplanada),
    tipo_de_oculos(X, escuros),
    bebida(X, champanhe).
tem_alma(X):-
    galinha_com_pele(X),
    estado_pele(X, tostada),
    bebida(X, caipirinha),
    tipo_de_veu(X, negro).


if (\+ tem_alma(X) and
    sentado(X, esplanada) and
    tipo_de_oculos(X, escuros) and
    bebida(X, champanhe)
) then (
    assert(tem_alma(X))
).

if (\+ tem_alma(X) and
    estado_pele(X, tostada) and
    bebida(X, caipirinha) and
    tipo_de_veu(X, negro)
) then (
    assert(tem_alma(X))
).



if (javali(X) and \+ bebida(X, champanhe))
then (pedir_bebida(X, champanhe)).

if (galinha_com_pele(X) and \+ bebida(X, caipirinha))
then (pedir_bebida(X, caipirinha)).
*/

/*
Escreve o predicado last/3, tal que last(L1, L2, X) significa que L2 é a lista de todos os elementos de L1, exceto o último, o qual é X, por exemplo

? - last([], L, X).
false
? - last([a, b, c], L, X).
L = [a, b] X = c;
false

Deve garantir-se que o primeiro argumento é uma lista, e que o segundo pode ser uma lista ou uma variável.
*/

l([X], [], X).
l([H|L], [H|L1], X):-
    l(L, L1, X).
last(L, L1, X):-
    is_list(L),
    (is_list(L1); var(L1)),
    l(L, L1, X).


/*
Usando o predicado type/2, que devolve o tipo de dados de um termo, define o predicado types/2, tal que types(L, Types) significa que Types é a lista, sem repetições, dos tipos
dos elementos da lista L, por exemplo

? - types([1, b, [], c, d, [x, y, z], 3.5], L).
L = [number, atom, list];
False

Não podes usar o nodups/2, mas podes inspirar-te na sua definição.

*/

type(X, number):- number(X), !.
type(X, list):- is_list(X), !.
type(X, var):- var(X), !.
type(X, atom):- atom(X), !.
 % com lista auxiliar
types(L, Types):-
    types(L, [], T),
    reverse(T, Types).
types([], Types, Types).
types([H|L], L1, Types):-
    type(H, Type),
    \+ member(Type, L1),
    types(L, [Type|L1], Types).
types([H|L], L1, Types):-
    type(H, Type),
    member(Type, L1),
    types(L, L1, Types).


% sem lista auxiliar
/*
types([], []).
types([H|L], [T|Types]):-
    type(H, T),
    types(L, Types),
    \+ member(T, Types).
types([H|L], Types):-
    type(H, T),
    types(L, Types),
    member(T, Types).
*/



/*
Imagina que tens um conjunto de listas de termos Prolog, mantidas em factos do predicado terms_list/1, por exemplo 

terms_list([1, b, [], c, d, [x, y, z], 3.5]).
terms_list([a, b]).

Escreve o procedimento print_types/0 que imprime, no ecrã do computador, as listas de tipos de dados dos elementos de cada uma dessas listas, por exemplo:

?- print_types.
[1, b, [], c, d, [x, y, z], 3.5]
    number
    atom
    list
[a, b]
    atom
true

Para isso, admite que dispões do predicado types/2 (pergunta 6) já implementado e que podes e deves usar. Podes usar qualquer outro predicado existente em Prolog.
*/

terms_list([1, b, [], c, d, [x, y, z], 3.5]).
terms_list([a, b]).

print_types:-
    terms_list(L),
    types(L, Types),
    writeL(L),
    member(T, Types),
    writeT(T),
    fail.
print_types.

writeL(L):- write(L), nl.
writeT(T):- tab(4), write(T), nl.

writeTypes(Types):-
    member(T, Types),
    write(T), nl,
    fail.
writeTypes(_).
    














