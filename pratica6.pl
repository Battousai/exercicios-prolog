:- ensure_loaded('maquina_robot_base.pl').

if (
    objectivo(bateria_carregada) and
    \+ ligada and
    \+ bateria_carregada
) then (
    remover_bateria,
    carregar_bateria,
    remover_ojectivo(bateria_carregada)
).

if (
    objectivo(bateria_inserida) and
    \+ ligada and
    \+ bateria_inserida and
    bateria_carregada
) then (
    inserir_bateria,
    remover_objectivo(bateria_inserida)
).

if (
    objectivo(ligada) and
    \+ ligada and
    bateria_inserida and
    bateria_carregada
) then (
    ligar,
    remover_objectivo(ligada)
).







