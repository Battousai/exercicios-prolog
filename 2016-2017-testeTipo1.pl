:- ensure_loaded('./psys.pl').

% calcular média com regras de produção

if (objectivo(media_feita) and \+ init)
then (
  assert(init),
  assert(count(0)),
  assert(sum(0))
).
if (\+ objectivo(media_feita) and init)
then (
  retract(init),
  retract(count(_)),
  retract(sum(_)),
  retract(input(_))
).

if (init and \+ input(_))
then (
  write('insira um número ou \'end\': '), nl,
  read(I),
  assert(input(I))
).

if (init and input(end) and count(C) and C > 0)
then (
  sum(S),
  M is S / C,
  write(M), nl,
  retract(objectivo(media_feita))
).

if (init and input(end) and count(C) and C = 0)
then (
  retract(objectivo(media_feita))
).

if (init and input(I) and number(I))
then (
  write('processando '), write(I), nl,
  assert(contar),
  sum(S),
  S1 is S + I,
  retract(sum(S)),
  assert(sum(S1)),
  count(C),
  C1 is C + 1,
  retract(count(C)),
  assert(count(C1)),
  retract(input(I))
).
