/*
*/

% This must be changed according to PSys location
:- ensure_loaded('./psys.pl').

% Sorting system
%
% Pergunta 1
% System interface
tidy_boxes(CurrentPositions, DesiredPositions):-
	 % Removes all control facts as well as all content facts
	clearMemory(to_be_removed),
	% Places the boxes on their original positions
	set_current_positions(CurrentPositions),
	set_desired_positions(DesiredPositions),
	psys.

% Facts to be removed by the clearMemory/1 procedure: clearMemory(to_be_removed)
%
to_be_removed([current_position/2, desired_position/2]).


% Actions
%
remove_box(Box, Pos):-
	retract(current_position(Box, Pos)),
	writelist(['Removed ', Box, ' from ', Pos]), nl.

place_box(Box, Pos):-
	assert(current_position(Box, Pos)),
	writelist(['Placed ', Box, ' in ', Pos]), nl.


% set_current_positions(DesiredPosiitons)
set_current_positions([(Box, Pos)|Positons]):-
       assert(current_position(Box, Pos)),
       set_current_positions(Positons).
set_current_positions([]).


% set_desired_positions(DesiredPosiitons)
set_desired_positions([(Box, Pos)|Positons]):-
       assert(desired_position(Box, Pos)),
       set_desired_positions(Positons).
set_desired_positions([]).



% Pergunta 2: sort numbers
%
sort_numbers(ListOfNumbers):-
	retractall(number(_, _)),
	store_list_of_numbers(1, ListOfNumbers),
	psys.

store_list_of_numbers(N, [X|Numbers]):-
	assert(number(X, N)),
	N1 is N + 1,
	store_list_of_numbers(N1, Numbers).
store_list_of_numbers(_, []).


% Pergunta 3: display sorted number
%
display_sorted_numbers:-
	assert(current_index(1)),
	psys.


% Folder flatening system

% System interface
flatten_folder(Folder):-
	clearMemory(facts_to_be_removed), % Removes all control facts as well as all content facts
	restore_content,            % Places the files on their original folders
	assert(initial_folder(Folder)),
	assert(current_folder(Folder)),
	assert(unfinished(Folder)),
	psys.

% Facts to be removed by the clearMemory/1 procedure: clearMemory(to_be_removed)
%
facts_to_be_removed([current_folder/1, initial_folder/1, unfinished/1, explored/1, content/2]).


% Places all files on their original folders
restore_content:-
	content_map(Map),
	restore_content(Map).

restore_content([(Folder, File)|Map]):-
	assert(content(Folder, File)),
	restore_content(Map).
restore_content([]).



% Files stored on each folder
content_map([
    (root,f1), (root,f2),
    (dir1,f11), (dir1,f12), (dir1,f13),
    (dir2,f21),
    (dir3,f31), (dir3,f32), (dir3,f33),
    (dir11,f111), (dir11,f112),
    (dir12,f121), (dir12,f122), (dir12,f123),
    (dir31,f311), (dir31,f312),
    (dir32,f321),
    (dir33,f331),
    (dir111,f1111), (dir111,f1112),
    (dir112,f1121), (dir112,f1122),
    (dir321,f3211), (dir321,f3212),
    (dir322,f3221),
    (dir1111,f11111), (dir1111,f11112),
    (dir3211,f32111), (dir3211,f32112)]
).



% Actions
%
% This action changes the current folder to the specified folder
% It also marks the specified folder as "unfinished" (meaning there
% are still subfolders and/or files to be processed
% go_to(Folder) makes sure that only one unfinished/1 fact exists for
% the specified folder
%
go_to(Folder):-
	retract(current_folder(_)),
	assert(current_folder(Folder)),
	record_unfinished_folder(Folder),
	writelist(['I have just arrived to folder ', Folder]), nl.

record_unfinished_folder(Folder):-
	retract(unfinished(Folder)),
	!,
	assert(unfinished(Folder)).
record_unfinished_folder(Folder):-
	assert(unfinished(Folder)).


% This action moves all files of the current folder to the specified
% folder
%
move_all_files(Folder, Parent):-
	retract(content(Folder, F)),
	assert(content(Parent, F)),
	writelist(['Moving ', F, ' to ', Parent]), nl,
	fail.
move_all_files(_, _).



% Folder hierarchy
%
subfolder(root, dir1).
subfolder(root, dir2).
subfolder(root, dir3).

subfolder(dir1, dir11).
subfolder(dir1, dir12).

subfolder(dir3, dir31).
subfolder(dir3, dir32).
subfolder(dir3, dir33).

subfolder(dir11, dir111).
subfolder(dir11, dir112).

subfolder(dir111, dir1111).

subfolder(dir32, dir321).
subfolder(dir32, dir322).

subfolder(dir321, dir3211).





% reservoir dog
% Reservoir dog interface
reservoir(Level, Buckets):-
	clearMemory(to_be_cleaned),
	assert(desired_level(Level)),
	assert(reservoir_level(100)),
	set_bucket_information(Buckets),
	psys.

to_be_cleaned([desired_level/1, reservoir_level/1, bucket_state/2, bucket_capacity/2]).

set_bucket_information(Buckets):-
	set_bucket_information(1, Buckets).

set_bucket_information(N, [(Cap, State)|Buckets]):-
	bucket_name(N, Bucket),
	assert(bucket_capacity(Bucket, Cap)),
	assert(bucket_state(Bucket, State)),
	N1 is N + 1,
	set_bucket_information(N1, Buckets).
set_bucket_information(_, []).

bucket_name(N, Bucket):-
	atomic_list_concat([bucket, N], Bucket).


% Reservoir dog actions
fill_bucket(Bucket):-
	retract(bucket_state(Bucket, empty)),
	assert(bucket_state(Bucket, full)),
	writelist(['Filling ', Bucket, '.']), nl.

dump_bucket(Bucket):-
	bucket_capacity(Bucket, C),
	retract(bucket_state(Bucket, full)),
	assert(bucket_state(Bucket, empty)),
	writelist([Bucket, ' was dumped into the reservoir. ', Bucket, ' is empty now']), nl,
	retract(reservoir_level(N)),
	N1 is N + C,
	assert(reservoir_level(N1)),
	writelist(['New reservoir level: ', N1]), nl.









