/*
Candidatos para efectuarem uma tarefa


*/

% Base de dados: tarefas
%
% Teste sistematico de programas de computador
% Introducao de dados num sistema de documentacao (intrd)
% Secretariado (secr)

tarefa(testp).
tarefa(intrd).
tarefa(secr).

%Horas necessarias para as tarefas
nHoras(tesp, 20).
nHoras(intrd, 25).
nHoras(secr, 15).

% Base de dados: Empregados: Alves, Matos

% Alves
trabalhador(alves).
horas_contratuais(alves, 40).
horas_atribuidas(alves, 20).
permitido(alves, testp).
capacidade(alves, testp).
interesse(alves, testp).


% Matos
trabalhador(matos).
horas_contratuais(matos, 40).
horas_atribuidas(matos, 15).
permitido(matos, secr).
capacidade(matos, secr).
interesse(matos, secr).

% Regras para seleccionar candidatos

candidato(A, T) :-
	trabalhador(A),
	tarefa(T),
	disponivel(A, T),
	capacidade(A, T),
	interesse(A, T).

disponivel(A, T) :-
	tempo_disp(A, T),	% Tempo disponivel
	permitido(A, T).	% E permitido A efectuar T

tempo_disp(A, T) :-
	horas_contratuais(A, N),
	horas_atribuidas(A, M),
	Disponivel is N - M,
	nHoras(T, HorasTarefa),
	Disponivel >= HorasTarefa.











actividade_da_tarefa(secr, dactilografar).
actividade_da_tarefa(secr, atender).
actividade_da_tarefa(secr, telefonar).

actividade_da_tarefa(intrd, upload).

actividade_da_tarefa(testp, executarProgramas).
actividade_da_tarefa(testp, monitorizar).
actividade_da_tarefa(testp, criarRelatorio).
actividade_da_tarefa(testp, roubar).

competencia(alves, executarProgramas).
competencia(alves, monitorizar).
competencia(alves, criarRelatorio).

competencia(matos, dactilografar).
competencia(matos, telefonar).
competencia(matos, atender).

actividade_proibida(roubar).



% tem competencia para actividade de tarefa?
incAct(Quem, Act):-
    \+ competencia(Quem, Act).
cmpAct(Quem, Act):-
    \+ incAct(Quem, Act).

% tem competencia para uma tarefa?
incTarefa(Quem, Tarefa):-
    actividade_da_tarefa(Tarefa, Act),
    \+ competencia(Quem, Act).
cmpTarefa(Quem, Tarefa):-
    \+ incTarefa(Quem, Tarefa).
















