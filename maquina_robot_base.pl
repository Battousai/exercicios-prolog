/*

M�quina Fotogr�fica Robotizada

Ficheiro de base para usar para a implementa��o de Sistema de Regras de Produ��o
para o Controlo de uma M�quina Fotogr�fica Robotizada

2010/11/02

*/
 

:- ensure_loaded('psys.pl').


% Programa de controlo da m�quibna robotizada

operar_maquina(Objectos, Estado):-
	write('A limpar a mem�ria.....'), nl,
	clearMemory,
	write('Criar objectivos de fotografar objectos ....'), nl,
	processar_objectos_a_fotografar(Objectos),
	write('Inserir a definiu��o do estado ...'), nl,
	processar_estado(Estado),
	write('Fim da inicializa��o...'), nl, nl,
	psys.


processar_objectos_a_fotografar([Obj|Objectos]):-
	criar_objectivo(fotografado(Obj)),
	processar_objectos_a_fotografar(Objectos).
processar_objectos_a_fotografar([]).


processar_estado([E|Estado]):-
	assert(E),processar_consequencias(E),
	processar_estado(Estado).
processar_estado([]).


processar_consequencias(ligada):-
	assert(bateria_carregada),
	assert(bateria_inserida),
	!.

processar_consequencias(focado(_)):-
	assert(ligada),
	processar_consequencias(ligada),
	!.
processar_consequencias(_).



% Defini��o de testes
test1 :-
	operar_maquina([1,2], [ligada]).

test2 :-
	operar_maquina([1,2], []).

test3 :-
	operar_maquina([1,2], [focado(2)]).

test4 :-
	operar_maquina([1,2], [bateria_inserida]).




% Acc�es ligadas � opera��o da m�quina

fotografar :-
	focado(X),
	assert(fotografado(X)),
	writelist(['Fotografar ', X, '.']), nl.


focar(X):-
	retract(focado(_)),
	!,
	assert(focado(X)),
	writelist(['Focar ', X, '.']), nl.

focar(X):-
	assert(focado(X)),
	writelist(['Focar ', X, '.']), nl.


ligar:-
	assert(ligada),
	write('Ligar.'), nl.


inserir_bateria:-
	assert(bateria_inserida),
	write('Inserir a bateria.'), nl.


carregar_bateria:-
	assert(bateria_carregada),
	write('Carregar a bateria.'), nl.


remover_bateria:-
	retract(bateria_inserida),
	write('Remover a bateria.'), nl.


criar_objectivo(P):-
	assert(objectivo(P)),
	writelist(['Criar o objectivo de atingir uma situa��o em que ', P, ' � verdade.']), nl.


remover_objectivo(P):-
	retract(objectivo(P)),
	writelist(['Remover o objectivo de atingir uma situa��o em que ', P, ' � verdade.']), nl.



% Ac��es relativas � especifica��o do estado inicial (interac��o com o utilizador)
/*
perguntar_se(P, Resposta)

processar_resposta(P, Resposta)

armazenar(P)

assert(estado_especificado).

retract(estado_conhecido(E)).

*/

perguntar_se(P, Resposta):-
	writelist([P, ' eh verdade?']), nl,
	repeat,
		write('sim/nao: '),
		read(Resposta),
		member(Resposta, [sim, nao]), !.

processar_resposta(P, sim):-
	assert(P),
	assert(estado_conhecido(P)).
processar_resposta(P, nao):-
	assert(estado_conhecido(P)).

armazenar(P):-
	assert(P),
	assert(estado_conhecido(P)).



% Book keeping and stuff
clearMemory :-
	abolish(ligada/0),
	abolish(focado/1),
	abolish(fotografado/1),
	abolish(bateria_inserida/0),
	abolish(bateria_carregada/0),
	abolish(objectivo/1).


writelist([X|Rest]):-
	write(X),
	writelist(Rest).
writelist([]).

