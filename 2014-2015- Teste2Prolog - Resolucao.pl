/*
Escreve o predicado list_count/2 que recebe uma lista e computa o seu comprimento. Exemplo:

?- list_count([], X).
X = 0
?- list_count([a, b, [1, 2], c], X).
X = 4

Não se pode usar o predicado length/2. A solução deve garantir que o primeiro argumento é uma lista e o segundo é um inteiro não negativo ou uma variável.
*/

list_count(L, C):-
    is_list(L),
    (integer(C), C >= 0;
    var(C)),
    !,
    lc(L, C).
lc([_|L], C):-
    lc(L, I),
    C is I + 1.
lc([], 0).

/*
Escreve o predicado oAttrib/4 que recebe informação sobre valores de vários atributos de vários objetos e devolve o atributo especificado de um objeto.

oAttrib(Information, Object, Attribute, X) significa que, de acordo com a informação em Information, X é o valor do atributo Attribute do objeto Object. O argumento Information é uma lista com informação sobre vários objetos. Cada elemento da lista diz respeito aos valores de um dado atributo de todos os objetos considerados.

Por exemplo, a lista [cor:[o1:verde, o2:vermelho], formato:[o2:triângulo, o1:retângulo], cheiro:[o1:relva]] representa informação sobre os objetos o1 e o2. o1 é um retângulo verde com cheiro a relva. o2 é um triângulo vermelho sem informação sobre o cheiro.

Exemplo:
?- oAttrib([peso:[o2:5, o1:1], cheiro:[o1:relva]], o2, peso, X). 

X = 5

Não faças validações dos argumentos. Nota: pensa-se que a informação sensorial do nosso cérebro está organizada de uma forma semelhante a esta.

R:
oAttrib(Information, Object, Attribute, Value):-
    member(Attribute:AttribInfo, Information),
    member(Oject:Value, AttribInfo).
*/

oAttrib([], _, _, _):- fail.
oAttrib([Attr:Objs|_], Obj, Attr, Res):-
    oObj(Objs, Obj, Res).
oAttrib([A:_|L], Obj, Attr, Res):-
    A \= Attr,
    !,
    oAttrib(L, Obj, Attr, Res).
oObj([Obj:Res|_], Obj, Res).
oObj([O:_|L], Obj, Res):-
    O \= Obj, !,
    oObj(L, Obj, Res).
oObj([], _, _):- fail.

oAttr(Information, Object, Attribute, Value):-
    member(Attribute:AttribInfo, Information),
    member(Object:Value, AttribInfo).


/*
Escreve o predicado homogeneous_list_with_exception/5 para gerar listas de elementos todos iguais exceto um deles. homogeneous_list_with_exception(N, E, I, X, List) significa que List é uma lista com N elementos, os quais são todos iguais a E, exceto o elemento de índice I, que é igual a X. Exemplo:

?- homogeneous_list_with_exception(4, a, 1, b, L).
L = [b, a, a, a]

*/

homogeneous_list_with_exception(N, E, I, X, [E|L]):-
    N >= 1, I \= 1, !,
    N1 is N - 1, I1 is I - 1,
    homogeneous_list_with_exception(N1, E, I1, X, L).
homogeneous_list_with_exception(N, E, 1, X, [X|L]):-
    N >= 1, !,
    N1 is N - 1,
    homogeneous_list_with_exception(N1, E, 0, X, L).
homogeneous_list_with_exception(0, _, _, _, []).


/*
Escreve o predicado identity_natrix/2 para gerar a matriz identidade com a dimensão especificada. Uma matriz identidade é uma matriz quadrada com todos os elementos iguais a 0, exceto os da diagonal principal, os quais são iguais a 1. Uma matriz é representada por uma lista de listas. Cada uma das listas da matriz representa uma linha.

Exemplo:

?- identity_natrix(3, M).
M = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

Não faças validações dos argumentos.

Sugestão: usa o predicado homogeneous_list_with_exception/5 para gerar cada uma das linhas da matriz identidade.

*/

identity_natrix(N, M):-
    in(1, N, M).
in(N, R, []):- N > R.
in(N, R, [S|M]):-
    N =< R,
    N1 is N + 1,
    in(N1, R, M),
    homogeneous_list_with_exception(R, 0, N, 1, S).
iden_matr(N, M):-
    id(N, N, [], M).
id(0, _, M, M):- !.
id(N, R, L, M):-
    N \= 0, !,
    N1 is N - 1,
    homogeneous_list_with_exception(R, 0, N, 1, S),
    id(N1, R, [S|L], M).

/*

Escreve o procedimento print_object_description/1 que recebe o identificador de um objeto e imprime, no ecrã do computador, a informação disponível sobre os seus atributos.

Exemplo:

?- print_object_description(o3).
cor(o3) = vermelho
peso(o3) = 2.5
formato(o3)= retângulo
true

A informação disponível sobre os objetos é representada através de uma lista como a da pergunta 2, armazenada no facto do predicado object_information/1, por exemplo

object_information([peso:[o2:5, o1:1], cor:[o1:vermelho]]).

Cada atributo possível está especificado num facto do predicado atribute/1, por exemplo

atribute(cor).
atribute(peso).
atribute(formato).
…

Não faças validações.

Sugestão: usa o predicado oAttrib/4 para obter o valor de cada atributo do objeto. Sugestão: se te der jeito, podes usar o procedimento writelist/1 para imprimir os vários elementos de uma lista.
*/

object_information([peso:[o2:5, o1:1], cor:[o1:vermelho]]).
object_information([peso:[o2:5, o1:50], cor:[o1:azul]]).
attribute(cor).
attribute(peso).
attribute(formato).



print_object_description(Obj):-
    object_information(Info),
    !,
    attribute(Attr),
%    writeObj(Obj, Attr, Info),
    oAttr(Info, Obj, Attr, Val),
    write(Attr), write(" = "), write(Val), nl,
    fail.
print_object_description(_).

 
writeObj(Obj, Attr, Info):-
    oAttr(Info, Obj, Attr, Val),
    write(Attr), write(" = "), write(Val), nl
    .

/*    object_information(Info),
    oAttr(Info, Obj, Attr, Val),
    write(Attr), write(" = "), write(Val), nl,
    fail.*/

    
/*

Escreve o procedimento describe_objects/0 que descreve, no ecrã do computador, os objetos indicados pelo utilizador. O procedimento pede o nome de um objeto ao utilizador e descreve esse objeto. O procedimento repete este processo enquanto o utilizador não introduzir a palavra fim, em vez do nome de um objeto.

A impressão da descrição do objeto recorre ao predicado print_object_description/.

Exemplo:

?- describe_objects.

Objeto (ou fim, para terminar)> o3.
cor(o3) = vermelho
peso(o3) = 2.5
formato(o3)= retângulo

Objeto (ou fim, para terminar)> fim.
true
*/

describe_objects:-
    repeat,
        write('Objecto (ou fim, para terminar): '),
        read(Obj),
        write('coiso'),nl,
        (Obj = fim; print_object_description(Obj), fail),
%        do(Obj),
    !.































