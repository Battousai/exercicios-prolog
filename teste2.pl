% symmetric(a, X) tal que X = z
% symmetric(b, X) tal que X = y
% entao escrever symmetric_list(List, Output) Output lista de
% simetricos
% solucao simulada com somas

symmetric_list([H|L], [C|O]):-
    symmetric(H, C),
    symmetric_list(L, O).
symmetric_list([], []).
symmetric(In, Out):-
    Out is In + 1.

% list_count(List, N) tal que N tamanho da lista

list_count([], 0).
list_count([_|L], N):-
    list_count(L, Next),
    N is Next + 1.




% nifs

%   nifs:-
%       repeat,
%           ler_pessoa(Nome),
%           processar_pessoa(Nome),
%       !.

nif(joana, 123123).
nif(joaquim, 3232).
nif(bleu, 9999).

processar_pessoa(fim):- !.
processar_pessoa(Nome):-
    nif(Nome, Nif),
    write(Nif), nl,
    fail.



% produtos
% id, tipo, preco
product(cup1, cup, 10).
product(p1, plate, 7).
product(cup2, cup, 5).
product(g1, glass, 3).

print_products(Type):-
    product(Prod, Type, Price),
    print_product(Prod, Price),
    fail.
print_products(_).

print_product(Prod, Price):-
    write(Prod),write(": "),write(Price),nl.

pp(Type):-
    product(Prod, Type, Price),
    write(Prod),write(": "),write(Price),nl,
    fail.
pp(_).



% polinomiais
maximum_degree(D1, D2, D1):-
    D1 >= D2,
    !.
maximum_degree(D1, D2, D2):-
    D2 > D1,
    !.

%polynomial_degree(Var, Poly, Degree):-
 
polynomial(Poly1 + Poly2, Deg):-
    polynomial(Poly1, Deg1),
    polynomial(Poly2, Deg2),
    maximum_degree(Deg1, Deg2, Deg).
polynomial(Poly1 - Poly2, Deg):-
    polynomial(Poly1, Deg1),
    polynomial(Poly2, Deg2),
    maximum_degree(Deg1, Deg2, Deg).
polynomial(Var, _*Var^N, N).
polynomial(Var, _*Var, 1).
polynomial(Var, Var^N, N).
polynomial(Var, Var, 1).

polynomial(Var, K, 0):-
    atomic(K),
    K \= Var.
































