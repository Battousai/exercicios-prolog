homem(joaquim).
homem(andrade).
homem(mauro).

mulher(andreia).
mulher(rita).
mulher(patricia).

filho(joaquim, andreia).
filho(rita, andreia).
filho(patricia, mauro).
filho(patricia, joaquim).

filho(mauro, rita).
filho(mauro, andrade).

filho(fonix, faca).

irmaosPorParteDoPai(Pai, Irmao):-
    homem(Pai),
    filho(Pai, Irmao).

irmaosPorParteDaMae(Mae, Irmao):-
    mulher(Mae),
    filho(Mae, Irmao).

irmaos(X, Y):-
    filho(Progenitor, X),
    filho(Progenitor, Y),
    X \= Y, X @< Y.

filho_unico(Filho, Progenitor):-
    filho(Progenitor, Filho),
    % para passar, verificar que não é possível provar que
    % existe outro filho diferente
    \+ (
        filho(Progenitor, OutroFilho),
        Filho \= OutroFilho
    )
    .
    
    
aula(gestao).
aula(matematica).
aula(evt).

aluno(mauro).
aluno(maria).
aluno(mafalda).
aluno(marco).

presenca(gestao, mauro).
presenca(matematica, mauro).
presenca(evt, mauro).

presenca(gestao, maria).
presenca(gestao, marco).

% ausente se existir alguma disciplina em que
% não é possível provar que aluno presenciou
ausente(Aluno):-
    aula(Disc),
    \+ presenca(Disc, Aluno).

% cromo se não se tiver ausentado nunca
cromo(Aluno):-
    \+ ausente(Aluno).
    
    
    
    

    
add(0,Y,Y).  
add(s(X),Y,s(Z)) :- add(X,Y,Z).
    
    
    
    
    
    
    
    
    
