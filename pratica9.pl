% nodups com cut

% versao mantem só inicial

nodups(L, O):- nodups(L, [], O).

nodups([], _, []):- !.
nodups([H|L1], L2, [H|L3]):-
    \+ member(H, L2),
    !,
    nodups(L1, [H|L2], L3).
nodups([H|L1], L2, L3):-
    member(H, L2),
    !,
    nodups(L1, L2, L3).


%versao mantem só última ocorrência
nd([H|L], [H|O]):-
    \+ member(H, L),
    nd(L, O),
    !.
nd([H|L], O):-
    member(H, L),
    nd(L, O),
    !.
nd([], []).



% evitar soluções duplicadas

% filho(Filho, Progenitor)
filho(ana, ze).
filho(ana, maria).
filho(luis, ze).
filho(luis, maria).
filho(sara, ana).
filho(sara, antonio).
filho(gil, ana).
filho(gil, pedro).

irmao(X, Y):-
    filho(X, Z),
    filho(Y, Z),
    X \= Y.

% usando os predicados homem/1, mulher/1, pessoa/1 e cut
% cria solução que gera todas as solucoes sem repeticoes
homem(ze).
homem(luis).
homem(antonio).
homem(gil).
homem(pedro).

mulher(ana).
mulher(maria).
mulher(sara).

pessoa(X):- homem(X); mulher(X).

    

% programa para adivinhar o número inteiro de 1 a 5,
% pensado pelo utilizador, até que o utilizador
% introduz a palavra "fim"
% no final devolve o número de acertos e de tentativas falhadas

guess(N):-
    assert(nguesses(0)),
    repeat,
        get_user_input(X),
        guess_number(X),
    !,
    retract(nguesses(N)).

get_user_input(X):-
    write('Write end to finish, or think of an integer between 1 and 5 and write ready> '),
    read(X).

guess_number(end).
guess_number(ready):-
    X is random(5) + 1,
    writelist(['My guess is ', X, '.']), nl,
    write('What number have you thought of? '),
    read(Number),
    update_number_of_guesses(Number, X). 
    
update_number_of_guesses(Number, X):-
    abs(Number-X) =< 1,
    retract(nguesses(G)),
    G1 is G + 1,
    assert(nguesses(G1)),
    !,
    fail.

writelist([X|L]):-
    write(X),
    writelist(L).
writelist([]).












