/*
Candidatos para efectuarem uma tarefa


*/

% Base de dados: tarefas
%
% Teste sistematico de programas de computador
% Introducao de dados num sistema de documentacao (intrd)
% Secretariado (secr)

tarefa(testp).
tarefa(intrd).
tarefa(secr).

%Horas necessarias para as tarefas
nHoras(tesp, 20).
nHoras(intrd, 25).
nHoras(secr, 15).

% Base de dados: Empregados: Alves, Matos

% Alves
trabalhador(alves).
horas_contratuais(alves, 40).
horas_atribuidas(alves, 20).
permitido(alves, testp).
capacidade(alves, testp).
interesse(alves, testp).


% Matos
trabalhador(matos).
horas_contratuais(matos, 40).
horas_atribuidas(matos, 15).
permitido(matos, secr).
capacidade(matos, secr).
interesse(matos, secr).

% Regras para seleccionar candidatos

candidato(A, T) :-
	trabalhador(A),
	tarefa(T),
	disponivel(A, T),
	capacidade(A, T),
	interesse(A, T).

disponivel(A, T) :-
	tempo_disp(A, T),	% Tempo disponivel
	permitido(A, T).	% E' permitido A efectuar T

tempo_disp(A, T) :-
	horas_contratuais(A, N),
	horas_atribuidas(A, M),
	Disponivel is N - M,
	nHoras(T, HorasTarefa),
	Disponivel >= HorasTarefa.

















