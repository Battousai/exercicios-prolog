% inteiros/2 inteiros(Lista, Inteiros) Inteiros list
% dos elementos Lista que são numeros inteiros

inteiros([], []).
inteiros([H|L], [H|O]):-
    number(H),
    inteiros(L, O).
inteiros([H|L], O):-
    \+ number(H),
    inteiros(L, O).

% media/2 media(Lista, Media)
% certificar que Lista é uma lista e que só tem numéricos
% media variavel ou numero

media(L, Media):-
    is_list(L),
    (var(Media); number(Media)),
    length(L, Len),
    Len > 0,
    doMedia(L, 0, Sum),
    Media is Sum / Len.

doMedia([H|L], Sum, Media):-
    NewSum is Sum + H,
    doMedia(L, NewSum, Media).
doMedia([], Sum, Sum).


% expansao/2 expansao(L, E) E expansao L.
% cada elemento de L tem a forma de um par (N, X)
% N: inteiro >= 0
% Exemplo:
% ?- expansao([(3, a), (1, b), (6, c)], L).
% L = [a, a, a, b, c, c, c, c, c, c]

expansao([(0, _)|_], []).
expansao([(Count, Char)|L], [Char|O]):-
    Count > 0,
    Next is Count - 1,
    expansao([(Next, Char)|L], O).
expansao(L, O):-
    





% max/2 max(Lista, Max) Max é o maior elemento de Lista

max([H|Lista], Max):-
    max(Lista, H, Max).
max([H|Lista], Current, Max):-
    H > Current,
    max(Lista, H, Max).
max([H|Lista], Current, Max):-
    H =< Current,
    max(Lista, Current, Max).
max([], Max, Max).




% insertion_sort/2 insertion_sort(L1, L2)
% L2 é o resultado de ordenar a lista L1 por ordem crescente
% para ordenar uma lista, basta inserir ordenadamente o seu
% primeiro elemento no resultado de ordenar o resto da
% lista. o resultado de ordenar uma lista vazia é uma lista vazia.

    




















