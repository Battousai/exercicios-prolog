% member/2 member(X, L) X membro da lista L
/*member(Nail, [Nail|_]).
member(Nail, [Head|List]):-
    Nail \= Head,
    member(Nail, List).*/

% append/3 append(L1, L2, L3) L3 concatenacao de L1 com L2
append([H|A], B, [H|C]):-
    append(A, B, C).
append([], B, B).

% first/2 first(X, L) X primeiro elemento de L
first(X, [X|_]).

% last/2 last(X, L) X último elemento de L
last(X, [X]):- !.
last(X, [_|L]):-
    last(X, L).

% reverse/2 reverse(L1, L2) L2 é L1 por ordem inversa
reverse(L1, L3):-
    reverse(L1, [], L3).
reverse([H|L1], L2, L3):-
    reverse(L1, [H|L2], L3).
reverse([], L2, L2).

% nodups/2 nodups(L1, L2) L2 = L1 sem duplicados
% só a primeira ocorrência é mantida
nodups([], []).
nodups(L1, L3):-
    nodups(L1, [], L3).
nodups([H|L1], L2, L3):-
    \+ member(H, L2),
    nodups(L1, [H|L2], L3).
nodups([H|L1], L2, L3):-
    member(H, L2),
    nodups(L1, L2, L3).
nodups([], L2, L2).

% só a segunda ocorrência é mantida
nd([],[]).
nd([H|L1], [H|L2]):-
    % verifica se elemento está lá
    % (está sempre na primeira ocorrência)
    \+ member(H, L1),
    nd(L1, L2).
nd([H|L1], L2):-
    % verifica se elemento está lá
    % (está sempre na primeira ocorrência)
    member(H, L1),
    nd(L1, L2).

    
% replace/4 replace(Old, New, OldList, NewList)
% substitui todas as ocorrencias de Old por New em OldList

replace(_, _, [], []).
replace(Old, New, [H|Ol], [New|Nl]):-
    H = Old,
    replace(Old, New, Ol, Nl).
replace(Old, New, [H|Ol], [H|Nl]):-
    H \= Old,
    replace(Old, New, Ol, Nl).
    
    
% nXList/3 nXList(N, X, L) L é lista com N elementos iguais a X

nXList(0, _, []).
nXList(N, X, [X|L]):-
    N > 0,
    Cont is N - 1,
    nXList(Cont, X, L).
    


% nList/2 nList(N, L) L lista com N primeiros naturais

nList(N, L):-
    nList(N, [], L).

nList(0, L, L).
nList(N, L, Out):-
    N > 0,
    Next is N - 1,
    nList(Next, [N|L], Out).

    
% nLeftList/3 nLeftList(N, L1, L2) L2 é lista formada pelos
% N elementos mais a esquerda de L1

nLeftList(0, _, []).
nLeftList(N, [H|L], [H|Out]):-
    N > 0,
    Next is N - 1,
    nLeftList(Next, L, Out).


% nRightList/3 nRightList(N, L1, L2) L2 é lista com N
% elementos mais a direita de L1

nrl(0, _, []).
nrl(N, L, O):-
    N > 0,
    reverse(L, X),
    nLeftList(N, X, O).

nRightList(N, L, Out):-
    length(L, Len),
    StartPos is Len - N,
    doList(StartPos, L, Out).
doList(StartPos, [_|L], Out):-
    StartPos > 0,
    NextPos is StartPos - 1,
    doList(NextPos, L, Out).
doList(0, L, L).
    
    























